
import {Container, Nav, Navbar, Image} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext, Fragment} from 'react';
import UserContext from '../UserContext.js';



export default function AppNavBar(){
	

const {user} = useContext(UserContext);
return(

		<Navbar bg="light" expand="lg" className ="appnavbar p-0" sticky="top">
		      <Container fluid className="navbar p-1">
		      	<Link to = "/">
		      	<Image className = "img-fluid logo col-2" src ="https://i.ibb.co/80t0C4g/logo.png"/>
		      	</Link>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		          <Nav.Link  as = {NavLink} to = "/">Home</Nav.Link>
		            
		            
		            {
		            	user ?
		            	<Fragment> 
		            	            {
		            	            	user.isAdmin ?
		            	            	<Nav.Link as = {NavLink} to = "/admin">Dashboard</Nav.Link>
		            	            	:
		            	            	<Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>

		            	            }
		            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
		            	</Fragment>
		            	:
		            	<Fragment>
		            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>

		            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		            	</Fragment>

		            }

		            
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>



	)

}