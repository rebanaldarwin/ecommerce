import {Form, Button} from 'react-bootstrap';
import {Fragment, useState, useContext, useEffect} from 'react';
import {useParams, useNavigate, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';



export default function UpdateProduct(){
	
	
	const navigate = useNavigate()
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const {user} = useContext(UserContext);
	const {productId} = useParams();

	useEffect(()=>{
		
		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data =>{
			

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
		})
	},[productId])
	
	function update(event){
	event.preventDefault()

		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/update/${productId}`,{
		method: 'PUT',
		headers:{
			'Content-Type': 'application/json',
			Authorization : `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
	})
		.then(result => result.json())
		.then(data =>{
		

		if(data === false ){
			
			Swal.fire({
                    title: "Updating failed!",
                    icon: "error",
                    text: "try again!"
                })
		}else{

		Swal.fire({
                    title: "Updating successful!",
                    icon: 'success',
                    text: "You updated this product!"
                })
		navigate('/admin');
		}
	})

	}

	return(
		user ?
		<Fragment>
		{
		user.isAdmin ?
		<Fragment>
			<h1 className = "mt-5 text-center">Update Product!</h1>
			<Form onSubmit = {event => update(event)} className="col-6 d-fluid text-center mx-auto mt-5 ">
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Product Name</Form.Label>
			        <Form.Control 
			        	className="text-center"
			        	type="text" 
			        	placeholder="product name"
			        	value ={name}
				       	onChange ={event => setName(event.target.value)}
			        	/>
			      </Form.Group>
			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Description</Form.Label>
			        <Form.Control 
			        	className="text-center"
			        	type="text" 
			        	placeholder="description" 
			        	value ={description}
				       	onChange ={event => setDescription(event.target.value)}
			        	/>
			      </Form.Group>
			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Price</Form.Label>
			        <Form.Control 
			        	className="text-center"
			        	type="text" 
			        	placeholder="price" 
						value ={price}
				       	onChange ={event => setPrice(event.target.value)}
			        	/>
			      </Form.Group>

			      <Button variant="primary" type="submit">
			        Update
			      </Button>
			    </Form>
		</Fragment>
		:
		<Navigate to = "*"/>
		}
		</Fragment>
		:
		<Navigate to = "*"/>
		)
}