import {Container, Row, Col, Carousel, Button, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';





export default function Banner(){


	return(
			<Container className="mt-5">
				<Row >
					<Col fluid = "true" className="text-center  col-10 h-50 mx-auto mt-3">
						<Image className = "img-fluid logo col-6 mb-3" src ="https://i.ibb.co/HC8BKcb/featured.png"/>
						<Carousel fade variant="dark" >
							
						      <Carousel.Item>
						        <img
						          className="d-block w-100 imgCar "
						          src="https://i.ibb.co/6FLCTJ2/bbsss1.jpg"
						          alt="Pastry Product"
						        />
						        <Carousel.Caption className="carouselCap col-6 mx-auto p-2">
						          <h5 className="mb-1">Blueberry Cheesecake</h5>
						          <p className="mb-1">Creamy cheesecake topped with blueberry filling and icing</p>
						        </Carousel.Caption>
						      </Carousel.Item>
						      <Carousel.Item>
						        <img
						          className="d-block w-100  imgCar"
						          src="https://i.ibb.co/z6v7vNs/318314420-194474619773808-8878461902045749508-n1.jpg"
						          alt="Second slide"
						        />

						        <Carousel.Caption  className="carouselCap col-6 mx-auto p-2">
						          <h5 className="mb-1">Chocobutternut Loaf</h5>
						          <p className="mb-1">moist chocolate loaf with butternut coating</p>
						        </Carousel.Caption>
						      </Carousel.Item>
						      <Carousel.Item>
						        <img
						          className="d-block w-100 imgCar"
						          src="https://i.ibb.co/yPsx4wn/262445210-129252872874197-8973948999358223742-n1.jpg"
						          alt="Pastry Product"
						        />

						        <Carousel.Caption  className="carouselCap col-6 mx-auto p-2">
						          <h5 className="mb-1">Classic Chocolate Cookies</h5>
						          <p className="mb-1">
						            Chewy cookies with high gquality chocolate bits
						          </p>
						        </Carousel.Caption>
						      </Carousel.Item>
						      <Carousel.Item>
						        <img
						          className="d-block w-100 imgCar"
						          src="https://i.ibb.co/qkBCtsL/260313466-129252892874195-2480269808023583860-n1.jpg"
						          alt="Pastry Product"
						        />

						        <Carousel.Caption  className="carouselCap col-6 mx-auto p-2">
						          <h5 className="mb-1">Banana Loaf with Dark Chocolate</h5>
						          <p className="mb-1">
						             moist banana loaf with dark chocolate and almond nuts toppings
						          </p>
						        </Carousel.Caption>
						      </Carousel.Item>

						      <Carousel.Item>
						        <img
						          className="d-block w-100 imgCar"
						          src="https://i.ibb.co/X322RX9/FB-IMG-1677056220584-01.jpg"
						          alt="Pastry Product"
						        />

						        <Carousel.Caption className="carouselCap col-6 mx-auto p-2">
						          <h5 className="mb-1">Customized Cake</h5>
						          <p className="mb-1">
						             Moist CHocolate cake with buttercream coat and design of your choice
						          </p>
						        </Carousel.Caption>
						      </Carousel.Item>
						    </Carousel>
						    <Button as = {Link} to = "/products" className = " btn mt-2 mb-5" variant="danger">Order here!</Button>
					</Col>
				</Row>
			</Container>
		








		)
}