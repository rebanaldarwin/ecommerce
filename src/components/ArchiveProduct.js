

import {useState, useEffect} from 'react';
import{Button, Modal} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';



export default function ArchiveProduct(){

	const navigate = useNavigate()
	const {productId} = useParams();

	const [isAvailable, SetIsAvailable]=useState('')


	useEffect(()=>{

		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data =>{
			console.log(data);
			
			if(data.isAvailable===true){
			SetIsAvailable(false)
		}else{
			SetIsAvailable(true)
		}
			
	})
}, [productId])

	function changeStatus(){
		
		
		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/toArchive/${productId}`,{
			method: 'PUT',
			headers:{
				'Content-Type': 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				
				isAvailable:isAvailable
				
			})
		})
		.then(result => result.json())
		.then(data =>{
			
			
			if(data === false ){
				Swal.fire({
	                    title: "Processing failed!",
	                    icon: "error",
	                    text: "Try again!"
	                })
			}else{
				
					Swal.fire({
			                    title: "transaction successful!",
			                    icon: 'success',
			                    text: "this product is now on archive!"
			                })
					navigate('/admin');
				}
		})
}


	return(
		<div
		      className="modal show"
		      style={{ display: 'block', position: 'initial' }}
		    >
		      <Modal.Dialog>
		        <Modal.Header closeButton>
		          <Modal.Title>Do you want to change the availability of this product?</Modal.Title>
		        </Modal.Header>

		        <Modal.Body>
		          <p>click save changes if yes</p>
		        </Modal.Body>

		        <Modal.Footer>
		          <Button  as = {Link} to = {`/admin`}  variant="secondary">Close</Button>
		          <Button onClick= {changeStatus} variant="primary">Save changes</Button>
		        </Modal.Footer>
		      </Modal.Dialog>
		    </div>
		)
}