import React from 'react';


const UserContext = React.createContext();


export const UserProvider = UserContext.Provider;

// The provider property of createContext
export default UserContext;

