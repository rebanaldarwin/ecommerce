import {Col, Row, Button, Table} from 'react-bootstrap';
import { Fragment, useContext, useEffect, useState} from 'react';
import ProductList from '../components/ProductList.js'
import {Navigate, Link} from 'react-router-dom';

import UserContext from '../UserContext.js';

export default function AdminDashboard (){



const {user} = useContext(UserContext);
const [allProducts, setAllProducts] = useState([]);
useEffect(()=>{

	fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/all`,{
		headers: {
				'Content-Type':'aplication/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
	})
	.then(result => result.json())
	.then(data =>{
		
		
		setAllProducts(data.map(product => {
	return(
		<ProductList key = {product._id}
		allProductProp = {product}/>
		)
		}))

	})

},[])




	return(
		user ?
		<Fragment>
		{
			user.isAdmin ?
			<Fragment>
				
				<h1 className = "text-center mb-0  mt-5 fxmargin">Admin Dashboard</h1>
				<Row  className = "mb-3 text-center">
					<Col>
						<Button as = {Link} to = {`/admin/addProduct`} variant="success" className = "m-1">Add New Product</Button>
						<Button variant="warning" className = "m-1">Show User Orders</Button>				
					</Col>
				</Row>
				<Table striped bordered hover variant="dark" className="mb-0 text-center" >
			      <thead>
			        <tr>
			          
			          <th className = " col-3">Product Name</th>
			          <th>Description</th>
			          <th className = " col-1">Price</th>
			          <th className = " col-2">Availability</th>
			          <th className = " col-1">Action</th>
			        </tr>
			      </thead>
		      	</Table>
				{allProducts}
				
				
			</Fragment>
			:
			<Navigate to = "*"/>
		}
		</Fragment>
		:
		<Navigate to = "*"/>
		)
}