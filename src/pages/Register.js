
import {Form, Button} from 'react-bootstrap';
import {Fragment, useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';



export default function Register(){

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const {user} =useContext(UserContext);
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();


useEffect(()=>{
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
				setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [email, password, confirmPassword])

function register(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/user/register`,{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			email: email,
			password: password,
			mobileNo: mobileNo
		})
	})
	.then(result => result.json())
	.then(data =>{
		console.log(data)

		if(data === false ){
			Swal.fire({
                    title: "Registration failed!",
                    icon: "error",
                    text: "Try again!"
                })
		}else{

		Swal.fire({
                    title: "Registration successful!",
                    icon: 'success',
                    text: "Welcome to Knead for Pastry!"
                })
		navigate('/login');
		}
	})
}



	return(
		user ?

		<Navigate to = "*"/>

		:
		<Fragment>
			<h1 className = "mt-5 text-center">Register</h1>
			<Form className = "mt-5 col-5 mx-auto  text-center" onSubmit = {event => register(event)}>
			     <Form.Group className="mb-3" controlId="formBasicFirstName">
			       <Form.Label>First name</Form.Label>
			       <Form.Control 
				       type="text" 
				        className="text-center"
				       placeholder="Enter first name"
				       value ={firstName}
				       onChange ={event => setFirstName(event.target.value)}
				       required
				        />
			     </Form.Group>

		         <Form.Group className="mb-3" controlId="formBasicLastName">
		            <Form.Label>Last name</Form.Label>
		            <Form.Control 
		     	       type="text" 
		     	        className="text-center"
		     	       placeholder="Enter last name"
		     	       value ={lastName}
		     	       onChange ={event => setLastName(event.target.value)}
		     	       required
		     	        />
		         </Form.Group>

			     <Form.Group className="mb-3" controlId="formBasicEmail">
			       <Form.Label>Email address</Form.Label>
			       <Form.Control 
				       type="email" 
				        className="text-center"
				       placeholder="Enter email"
				       value ={email}
				       onChange ={event => setEmail(event.target.value)}
				       required
				        />
			       <Form.Text className="text-muted">
			         We'll never share your email with anyone else.
			       </Form.Text>
			     </Form.Group>

		         <Form.Group className="mb-3" controlId="formBasicMobileNo">
		            <Form.Label>Mobile number</Form.Label>
		            <Form.Control 
		     	       type="number"
		     	        className="text-center" 
		     	       placeholder="Enter mobile number"
		     	       value ={mobileNo}
		     	       onChange ={event => setMobileNo(event.target.value)}
		     	       required
		     	        />
		         </Form.Group>

			     <Form.Group className="mb-3" controlId="formBasicPassword">
			       <Form.Label>Password</Form.Label>
			       <Form.Control 
			       type="password" 
			        className="text-center"
			       placeholder="Password"
			       value ={password}
			       onChange ={event => setPassword(event.target.value)}
			       required
			        />
			     </Form.Group>
			    <Form.Group className="mb-3" controlId="formConfirmBasicPassword">
			       <Form.Label>Confirm Password</Form.Label>
			       <Form.Control 
			       type="password" 
			        className="text-center"
			       placeholder="confirm your Password"
			       value ={confirmPassword}
			       onChange ={event => setConfirmPassword(event.target.value)}
			       required
			        />
			     </Form.Group>

			     {
			     	isActive ?
			     	<Button  variant="danger" type="submit">
			       	Submit
				    </Button>
				    :
		         	<Button variant="danger" type="submit" disabled>
		           	Submit
		    	    </Button>
			     }

			    
			   </Form>
		   </Fragment>
		)
}