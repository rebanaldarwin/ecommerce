import {Container, Row , Col, Image} from 'react-bootstrap';
import {Route} from 'react-router-dom';
import {Link} from 'react-router-dom';



export default function PageNotFound() {
	
	return (
		<Container>
			<Row className="text-center mx-auto">
				<Col>
				<Image className = "img-fluid img404 mt-5" src ="https://i.ibb.co/3CV1SGz/error-404.png"/>
				<h4 className ="text-center text-muted">Go back to the <Link to ={"/"}>homepage</Link>.</h4>

				</Col>
					
			</Row>

		</Container>

		)

}
