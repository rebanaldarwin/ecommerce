
import {Form, Button} from 'react-bootstrap';
import {Fragment, useContext,useState, useEffect} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';




export default function Login(){

	const [email, setEmail] = useState('');
	const[password, setPassword] = useState('');

	const navigate = useNavigate();



	

	

	const {user, setUser} =useContext(UserContext);




	const [isActive, setIsActive] = useState(false);



	useEffect(()=>{
	if (email !== "" && password !== "" ){
		setIsActive(true);
	}else(
		setIsActive(false)
		)

}, [email, password])

function login(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/user/login`,{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(result => result.json())
	.then(data =>{
		

		if(data === false){
			Swal.fire({
                    title: "Authentication failed!",
                    icon: "error",
                    text: "Please try again!"
                })

			
		}else{

		

		localStorage.setItem('token', data.auth);
		retrieveUserDetails(localStorage.getItem ('token'));
		
		Swal.fire({
                    title: "Authentication successful!",
                    icon: 'success',
                    text: "Welcome to Knead for Pastry!"
                })
		

		navigate('/');
		}
	})
	
}
const retrieveUserDetails = (token) =>{
	fetch(`http://localhost:3001/user/details`,{
		headers:{
			Authorization: `Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(data => {
		
		setUser({
			id:data._id,
			isAdmin: data.isAdmin
		})
	})
}



	return(
		user ?

		<Navigate to = "*"/>

		:


		<Fragment className = "vh-100">
			<h1 className = "mt-5 text-center">Login</h1>
			<Form className = "d-fluid text-center mx-auto mt-5 col-4 vh-100" onSubmit = {event => login(event)}>
			     <Form.Group className="mb-3" controlId="formBasicEmail">
			       <Form.Label>Email address</Form.Label>
			       <Form.Control 
			       		className = "text-center"
				       type="email" 
				       placeholder="Enter email"
				       value ={email}
				       onChange ={event => setEmail(event.target.value)}
				       required
				        />
			       <Form.Text className="text-muted">
			         We'll never share your email with anyone else.
			       </Form.Text>
			     </Form.Group>

			     <Form.Group className="mb-3" controlId="formBasicPassword">
			       <Form.Label>Password</Form.Label>
			       <Form.Control 
			       className = "text-center"
			       type="password" 
			       placeholder="Password"
			       value ={password}
			       onChange ={event => setPassword(event.target.value)}
			       required
			        />
			     </Form.Group>
			   
			     <h6 className ="text-center text-muted">No account yet? register <Link to ={"/register"}>here</Link>.</h6>
			     {
			     	isActive ?
			     	<Button  variant="danger" type="submit">
			       	Login
				    </Button>
				    :
		         	<Button  variant="danger" type="submit" disabled>
		           	Login
		    	    </Button>
			     }

			    	

			   </Form>
		   </Fragment>
		)
}