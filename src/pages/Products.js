
import ProductCard from '../components/ProductCard.js';
import {Fragment, useEffect, useState} from 'react';



export default function Products(){



const [products, setProducts] = useState([]);
useEffect(()=>{

	fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/allAvailable`)
	.then(result => result.json())
	.then(data =>{
		
		
		setProducts(data.map(product => {
	return(
		<ProductCard key = {product._id}
		productProp = {product}/>
		)
		}))

	})

},[])


	return(
		<Fragment>
			<h1 className = "text-center mt-3  vh-100">Products</h1>
			{products}
		</Fragment>
		


		)
}