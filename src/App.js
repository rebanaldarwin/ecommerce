 
import './App.css';
import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar.js';
import Footer from './components/Footer.js';
import Home from "./pages/Home.js";
import Register from "./pages/Register.js";
import Products from "./pages/Products.js";
import Login from "./pages/Login.js";
import Logout from './pages/Logout.js';
import AdminDashboard from "./pages/AdminDashboard.js";
import AddProduct from "./components/AddProduct.js";
import ProductView from "./components/ProductView.js";
import UpdateProduct from "./components/UpdateProduct.js";
import ArchiveProduct from "./components/ArchiveProduct.js";
import PageNotFound from "./pages/PageNotFound.js";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {UserProvider} from './UserContext.js';

function App() {


const [user, setUser] = useState(null);


useEffect(()=>{
  console.log(user);
},[user]);


const unSetUser = ()=>{
  localStorage.clear()
  setUser(localStorage.getItem('email'));
}


  return (
    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path='/' element ={<Home/>}/>
          <Route path='/products' element ={<Products/>}/>
          <Route path='/product/:productId' element ={<ProductView/>}/>
          
          <Route path='/register' element ={<Register/>}/>
          <Route path='/login' element ={<Login/>}/>
          <Route path = "/logout" element = {<Logout/>}/>
          <Route path='/admin' element ={<AdminDashboard/>}/>
          <Route path='/admin/addProduct' element ={<AddProduct/>}/>
          <Route path='/admin/updateProduct/:productId' element ={<UpdateProduct/>}/>
          <Route path='/admin/archiveProduct/:productId' element ={<ArchiveProduct/>}/>
          <Route path = "*" element = {<PageNotFound/>}/>
        </Routes>
        <Footer/>
      </Router>
    </UserProvider>
  );
}

export default App;
